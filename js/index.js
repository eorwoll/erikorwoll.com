$(document).ready(function() {
	// smooth scroll on link click
	$(function() {
	  $('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

	$("#formSubmit").on("click", function(event) {
		event.preventDefault();

		$(".form-alert").hide();

		var isValid = true;

		$('.form-control').each(function() {
			if ( $(this).val() === '' ) {
			    $(".form-alert").show();
			    isValid = false;
			    return false;
			}
		});
		if(isValid) {
			// Send email
			var data = {};
			data["name"] = $("#name").val();
			data["email"] = $("#email").val();
			data["subject"] = $("#subject").val();
			data["message"] = $("#message").val();
			
			$.post(
				'mail.php',
				data,
				function(response) {
					if(response === "success") {
						$(".alert").removeClass("alert-danger");
						$(".alert").addClass("alert-success");
						$(".form-alert").text("Message sent successfully!");
						$(".form-alert").show();
						$("#contactForm").hide();
					} else {
						$(".form-alert").text("An error occurred sending email.");
						$(".form-alert").show();
					}
				}
			);
		}
	});
});